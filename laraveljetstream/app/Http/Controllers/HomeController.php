<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\akunsiswa;
use App\Models\akunguru;
use App\Models\materibiologi;
use App\Models\TugasBiologi;
use App\Models\tugasfisika;
use App\Models\materifisika;
use App\Models\materikimia;
use App\Models\tugaskimia;

class HomeController extends Controller
{
    public function index()
    {
        
        $role=Auth::user()->role;

        if($role=='1') {
            return view ('admin');//kepala sekolah
        }
        if($role=='2') {
            $tugfis = tugasfisika::all();
            $data = TugasBiologi::all();
            $materi = materibiologi::all();
            return view('seller' , compact ('data' , 'materi' , 'tugfis'));//guru
        }
        else {
            $materi = materibiologi::all();
            $data = TugasBiologi::all();
            return view('dashboard' , compact ('data' , 'materi'));//siswa
        }
    }

    public function depan() {
        return view('welcome');
    }
    public function akunsiswa(Request $request) {
        $data = akunsiswa::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('fotosiswa/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect('/')->with('success','Success , Data Berhasil Di Tambahkan dan tunggu 1x24 jam akun akan dikirimkan via Email' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }
    public function akunguru(Request $request) {
        $data = akunguru::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('fotoguru/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect('/')->with('success','Success , Data Berhasil Di Tambahkan dan tunggu 1x24 jam akun akan dikirimkan via Email' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }
    public function biologi() 
    {
        $materi = materibiologi::all();
        $data = TugasBiologi::all();
        return view('biologi' , compact('data' , 'materi'));
    }
    public function tugasbiologi(Request $request)
    {
        $data = TugasBiologi::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/biologi')->with('success','Success , Tugas Biologi Berhasil Di Tambahkan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function materibiologi(Request $request)
    {
        $data = materibiologi::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/biologi')->with('success','Success , Materi Biologi Berhasil Di Tambahkan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function edittugas($id)
    {
        $data = TugasBiologi::find($id);
        return view('edittugas' , compact ('data'));
    }
    public function updatedata(Request $request , $id)
    {
        $data = TugasBiologi::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }

    public function insertmateri(Request $request , $id)
    {
        $data = materibiologi::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }

    public function updatemateri($id)
    {
        $data = materibiologi::find($id);
        return view('editbiologi' , compact ('data'));
    }
    public function updateberhasil()
    {
        return view('sukses');
    }
    public function deleted($id) {
        $data = TugasBiologi::find($id);    
        $data->delete();
        return view('hapus');
    }

    public function hapus($id) {
        $data = materibiologi::find($id);    
        $data->delete();
        return view('hapus');
    }

    public function kurikulumbiologi()
    {
        $tugas = TugasBiologi::all();
        $data = materibiologi::all();
        return view('kurikulumbiology' , compact('data' , 'tugas'));
    }

    public function fisika() 
    {
        return view('lamanfisika');
    }

    public function gurufisika() 
    {
        $fisika = materifisika::all();
        $data = tugasfisika::all();
        return view('teacherfisika' , compact('data' , 'fisika'));
    }

    public function tugasfisika(Request $request)
    {
        $data = tugasfisika::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/gurufisika')->with('success','Success , Tugas Fisika Berhasil Di Tambahkan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function editfisika($id)
    {
        $data = tugasfisika::find($id);
        return view('editfisika' , compact ('data'));
    }

    public function updatefisika(Request $request , $id)
    {
        $data = tugasfisika::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }
    public function trash($id)
    {
        $data = TugasBiologi::find($id);
        $data->delete();
        return view('hapus');
    }

    public function trashfisika($id)
    {
        $data = tugasfisika::find($id);
        $data->delete();
        return view('hapus');
    }

    public function trashmaterifisika($id)
    {
        $data = materifisika::find($id);
        $data->delete();
        return view('hapus');
    }

    public function buangmateri($id)
    {
        $data = materifisika::find($id);
        $data->delete();
        return view('hapus'); 
    }

    public function materiphysics(Request $request)
    {
        $data = materifisika::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/gurufisika')->with('success','Success , Materi Fisika Berhasil Di Tambahkan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function editmaterifisika($id)
    {
        $data = materifisika::find($id);
        return view('aturmaterifisika' , compact ('data'));
    }

    public function insertmaterifisika(Request $request , $id)
    {
        $data = materifisika::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }

    public function teachkimia()
    {
        $data = materikimia::all();
        return view('teachkimia' , compact('data'));
    }

    public function insertkimia(Request $request)
    {
        $data = materikimia::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/teachkimia')->with('success','Success , Materi Kimia Berhasil Di Tambahkan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function editmaterikimia($id)
    {
        $data = materikimia::find($id);
        return view('aturmaterifisika' , compact ('data'));
    }
    
    public function updatematerikimia(Request $request , $id)
    {
        $data = materikimia::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }
    
    

}
