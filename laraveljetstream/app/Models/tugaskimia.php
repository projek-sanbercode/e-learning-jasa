<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tugaskimia extends Model
{
    use HasFactory;
    protected $table = 'tugaskimia';
    protected $fillable = ['matapelajaran' ,'kelas', 'tanggalpublis' , 'tenggat' , 'file'];
}
