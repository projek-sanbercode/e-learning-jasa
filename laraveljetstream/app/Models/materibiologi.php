<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class materibiologi extends Model
{
    use HasFactory;
    protected $table = 'materibio';
    protected $fillable = ['kelas' ,'tanggalpublis', 'pengampu' , 'catatan' , 'file'];
}
