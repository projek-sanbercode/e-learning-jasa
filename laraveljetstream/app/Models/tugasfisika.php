<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tugasfisika extends Model
{
    use HasFactory;
    protected $table = "tugasfisika";
    protected $fillable = ['matapelajaran' ,'kelas', 'tanggalpublis' , 'tenggat' , 'file'];
}
