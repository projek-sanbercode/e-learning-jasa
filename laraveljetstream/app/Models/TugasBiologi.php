<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TugasBiologi extends Model
{
    use HasFactory;
    protected $table = "tugasbiologi";
    protected $fillable = ['matapelajaran' ,'kelas', 'tanggalpublis' , 'tenggat' , 'file'];
}
