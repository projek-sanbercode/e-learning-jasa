<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class materifisika extends Model
{
    use HasFactory;
    protected $table = 'materifisika';
    protected $fillable = ['kelas' ,'tanggalpublis', 'pengampu' , 'catatan' , 'file'];
}
