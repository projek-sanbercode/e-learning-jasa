<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class materikimia extends Model
{
    use HasFactory;
    protected $table = 'materichems';
    protected $fillable = ['kelas' ,'tanggalpublis', 'pengampu' , 'catatan' , 'file'];
}
