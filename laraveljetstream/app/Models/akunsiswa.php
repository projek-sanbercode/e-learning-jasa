<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class akunsiswa extends Model
{
    use HasFactory;
    protected $table = 'students';
    protected $fillable = ['namasiswa' ,'emailsiswa', 'nisn' , 'fotosiswa' , 'jurusan' , 'kelas' , 'guruwali'];
}
