  <!-- 
  Nama : Johannes Bastian Jasa Sipayung 
  NIM : 11422013
  Prodi : DIV-Teknologi Rekayasa Perangkat Lunak
  Tanggal : Rabu , 27 Januari 2023 
-->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KitaAja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-secondary">
  <nav class="navbar navbar-expand-lg bg-warning">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><strong>KitaAja</strong></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href=" ">Produksi</a>
        </li>
       </ul>
       <form class="d-flex" role="search">
       <?php
                echo date("l/d/m/Y, H:i");
            ?>
      </form>
    </div>
  </div>
</nav>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>

    <?php
    //Asociative Array
    $produksi = [
                    ["Tipe Mobil" => "Sport" , 
                     "Nama Mobil" => "Toyota 86" ,
                     "Stok" => 2 , 
                     "Harga Beli" => "Rp 700.000.000,00" ,
                     "Harga Jual" => "Rp 775.750.000,00" ,
                     "Dealer" => "KitaAja"] , 

                     ["Tipe Mobil" => "Sedan" , 
                     "Nama Mobil" => "New Vlus" ,
                     "Stok" => 3 , 
                     "Harga Beli" => "Rp 320.000.000,00" ,
                     "Harga Jual" => "Rp 324.950.000,00" ,
                     "Dealer" => "KitaAja"] , 

                     ["Tipe Mobil" => "SUV" , 
                     "Nama Mobil" => "Land Crulser" ,
                     "Stok" => 1 , 
                     "Harga Beli" => "Rp 2.100.000.000,00" ,
                     "Harga Jual" => "Rp 2.213.450.000,00" ,
                     "Dealer" => "KitaAja"] , 

                     ["Tipe Mobil" => "MPV" , 
                     "Nama Mobil" => "New Vellvire" ,
                     "Stok" => 1 , 
                     "Harga Beli" => "Rp 1.110.000.000,00" ,
                     "Harga Jual" => "Rp 1.210.650.000,00" ,
                     "Dealer" => "KitaAja"] , 

                     ["Tipe Mobil" => "Sport" , 
                     "Nama Mobil" => "Kawasaki Ninja H2" ,
                     "Stok" => 7 , 
                     "Harga Beli" => "Rp 550.000.000,00" ,
                     "Harga Jual" => "Rp 600.000.000,00" ,
                     "Dealer" => "KitaAja"],
    ];
        //Indexed Array
    $barang = array(
    array("Sport" , "Toyota 86" , "2" , "Rp 700.000.000,00" , "Rp 775.750.000,00" , "KitaAja"),
    array("Sedan" , "New Vlus" , "3"  , "Rp 320.000.000,00" ,"Rp 324.950.000,00" ,"KitaAja") , 
    array("SUV" , "Land Crulser" , "1" ,"Rp 2.100.000.000,00" ,"Rp 2.213.450.000,00" ,"KitaAja") ,
    array("MPV" , "New Vellvire" , "1" ,"Rp 1.110.000.000,00" ,"Rp 1.210.650.000,00" ,"KitaAja") ,
    array("Sport" , "Kawasaki Ninja H2" , "7" ,"Rp 550.000.000,00" ,"Rp 600.000.000,00" ,"KitaAja")
    );
              
    //multidimensional Array
    $multi = [
        ["Sport" , "Toyota 86" , 2 , "Rp 700.000.000,00" ,"Rp 775.750.000,00" ,"KitaAja"] , 

         ["Sedan" , "New Vlus" , 3 , "Rp 320.000.000,00" , "Rp 324.950.000,00" ,"KitaAja"] , 

         ["SUV" , "Land Crulser" , 1 ,  "Rp 2.100.000.000,00" , "Rp 2.213.450.000,00" , "KitaAja"] , 

         [ "MPV" ,  "New Vellvire" , 1 ,  "Rp 1.110.000.000,00" , "Rp 1.210.650.000,00" , "KitaAja"] , 

         [ "Sport" , "Kawasaki Ninja H2" , 7 ,  "Rp 550.000.000,00" , "Rp 600.000.000,00" , "KitaAja"],
];

    ?>
    
    <div class="container mt-5">
        <div class="container">
            <h3  class="text-light">Indexed Array</h3>
        </div>
    <table class="table bg-dark text-light">
  <thead>
    <tr>
      <th scope="col">Tipe Mobil</th>
      <th scope="col">Nama Mobil</th>
      <th scope="col">Stok</th>
      <th scope="col">Harga Beli</th>
      <th scope="col">Harga Jual</th>
      <th scope="col">Dealer</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($barang as $storage) { ?>
        <tr>
            <td><?php echo $storage[0]?></td>
            <td><?php echo $storage[1]?></td>
            <td><?php echo $storage[2]?></td>
            <td><?php echo $storage[3]?></td>
            <td><?php echo $storage[4]?></td>
            <td><?php echo $storage[5]?></td>
        </tr>
        <?php } ?>  
    
    
  </tbody>


</table>
    </div>

    <div class="container mt-5">
    <div class="container">
        <h3 class="text-light">Associative Array</h3>
    </div>
    <table class="table bg-dark text-light">
  <thead>
    <tr>
      <th scope="col">Tipe Mobil</th>
      <th scope="col">Nama Mobil</th>
      <th scope="col">Stok</th>
      <th scope="col">Harga Beli</th>
      <th scope="col">Harga Jual</th>
      <th scope="col">Dealer</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($produksi as $production) { ?>
        <tr>
            <td><?php echo $production["Tipe Mobil"]?></td>
            <td><?php echo $production["Nama Mobil"]?></td>
            <td><?php echo $production["Stok"]?></td>
            <td><?php echo $production["Harga Beli"]?></td>
            <td><?php echo $production["Harga Jual"]?></td>
            <td><?php echo $production["Dealer"]?></td>
        </tr>
        <?php } ?>  
    
    
  </tbody>
</table>
    </div>

    </table>
    </div>


    <h3 class="text-light container mt-5">Jumlah Harga Beli dan Jumlah Harga Jual</h3>
    <div class="container  bg-dark">
    <table class="table text-light">
  <thead>
    <tr>
      <th scope="col">Keterangan</th>
      <th scope="col">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Jumlah Harga Beli</td>
      <td>Rp 9.420.000.000,00</td>
    </tr>
    <tr>
      <td>Jumlah Harga Jual</td>
      <td>Rp 10.150.450.000,00</td>
    </tr>
    <tr>
      <th>Total</th>
      <th>Rp 730.450.000,00</th>
    </tr>
  </tbody>
</table>
    </div>
 

</html>