<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , [HomeController::class , 'depan']);
Route::get('/biologi' , [HomeController::class , 'biologi']);
Route::post('/tugasbiologi' , [HomeController::class , 'tugasbiologi']);
Route::post('/materibiologi' , [HomeController::class , 'materibiologi']);
Route::post('/akunsiswa' , [HomeController::class , 'akunsiswa']);
Route::post('/akunguru' , [HomeController::class , 'akunguru']);




Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('/redirects' , [HomeController::class , 'index']);
Route::get('/berhasil/{id}' , [HomeController::class , 'updateberhasil']);
Route::get('/edittugas/{id}' , [HomeController::class , 'edittugas']);
Route::post('/updatedata/{id}' , [HomeController::class , 'updatedata']);
Route::get('/deleted /{id}',[HomeController::class, 'deleted'] )->name('deleted');
Route::get('/hapus/{id}',[HomeController::class, 'hapus'] )->name('hapus');
Route::get('/updatemateri/{id}' , [HomeController::class , 'updatemateri']);
Route::post('/insertmateri/{id}' , [HomeController::class , 'insertmateri']);

Route::get('/kurikulumbio' , [HomeController::class , 'kurikulumbiologi']);
Route::get('/fisika' , [HomeController::class , 'fisika']);//laman siswa
Route::get('/gurufisika' , [HomeController::class , 'gurufisika']);//laman guru
Route::post('/tugasfisika' , [HomeController::class , 'tugasfisika']);//tambahmateri
Route::get('/editfisika/{id}' , [HomeController::class , 'editfisika']);//edittugas fisika
Route::post('/updatefisika/{id}' , [HomeController::class , 'updatefisika']);//updatetugas fisika
Route::get('/trash/{id}' ,[HomeController::class , 'trash']);
Route::get('/trashfisika/{id}' ,[HomeController::class , 'trashfisika']);
Route::post('/materifiska' , [HomeController::class , 'materiphysics']);
Route::get('/editmaterifisika/{id}' , [HomeController::class , 'editmaterifisika']);//editmatteri fisika
Route::post('/updatematerifisika/{id}' , [HomeController::class , 'updatematerifisika']);//updatetugas fisika
Route::post('/insertmaterifisika/{id}' , [HomeController::class , 'insertmaterifisika']);
Route::get('/trashmaterifisika/{id}' ,[HomeController::class , 'trashmaterifisika']);
Route::get('/buangmateri/{id}' ,[HomeController::class , 'buangmateri']);
Route::get('/teachkimia' ,  [HomeController::class , 'teachkimia'])->name('teachkimia');
Route::post('/insertkimia' , [HomeController::class , 'insertkimia'])->name('insertkimia');
Route::get('/editmaterikimia/{id}' , [HomeController::class , 'editmaterikimia']);//editmatteri fisika
Route::post('/updatematerikimia/{id}' , [HomeController::class , 'updatematerikimia']);//updatetugas fisika













